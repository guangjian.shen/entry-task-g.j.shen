import React, { Component } from 'react'
import { connect } from 'react-redux'
import './Toast.less'

interface Props {
    isToastShow: boolean
    toastText: string
}

export default class Toast extends Component<Props> {
    render() {
        return (
            <div className='toast' style={{display: this.props.isToastShow ? 'block' : 'none'}}>
                {this.props.toastText}
            </div>
        )
    }
}

