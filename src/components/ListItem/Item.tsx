import React, { Component } from 'react'
import { connect, history } from 'umi'
import {transformTime, debounce} from '@/utils/index'
import './Item.less'
import {ReactComponent as LogoTime} from '@/assets/svgs/time.svg'

interface Event {
    id: number,
    name: string,
    begin_time: string,
    end_time: string,
    description: string,
    images: Array<string>
    goings_count: number,
    likes_count: number,
    creator: {
      avatar: string,
      username: string
    },
    channel: {
      name: string,
      id: number 
    }
  }

interface Props {
    event: Event
    key: number
    //updateDetailEvent: Function
}

export default class Item extends Component<Props> {
    state = {
        event: []
    }

    goToDetails = (id: any) => {
      history.push(`/events/${id}`);
    }

    render () {
        const {event} = this.props
        return (
            <div key={event.id} className='event-item' onClick={() => this.goToDetails(event.id)}>
              <div className='event-item-header'>
                <div className='creator'>
                  <img src={event.creator.avatar} alt='avatar' className='creator-avator'/>
                  <span>{event.creator.username}</span>
                </div>
                <div className='channel-name'>
                  {event.channel.name}
                </div>
              </div>

              <div className='event-item-title-container'>
                <div className='event-item-title'>  
                  <div className='event-item-name'>
                    {event.name}
                  </div>
                  <div className='event-item-time'>
                    <LogoTime className='icon-time'></LogoTime>
                    <span className='time-span'>{transformTime(event.begin_time)} - {transformTime(event.end_time)}</span>
                  </div>
                </div>
                {event.images && event.images.length > 0 ? <img className='event-item-img' src={event.images[0]} /> : null}
              </div>
              
              <div className='event-item-description'>
                {event.description}
              </div>
              <div className='event-item-footer'>
                <span className='icon-check-outline'>  {event.goings_count} Going</span>
                <span className='icon-like-outline'>  {event.likes_count} Likes</span>
              </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch: Function) => {
  return {
    updateDetailEvent: (detailEvent: []) => {
        const action = {
            type: 'user/updateDetailEvent',
            payload: detailEvent
        }
        dispatch(action)
    },
  }
}
