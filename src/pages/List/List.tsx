import React, { Component } from 'react'
import { connect, Link, history } from 'umi'
import {reqUser, reqEvents, join} from '@/api/index'
import {transformTime, debounce} from '@/utils/index'
import {User} from '@/models/index'
import Search from '../Search/Search'
import Item from '@/components/ListItem/Item'
import Login from '@/pages/Login/Login'


import {ReactComponent as LogoCat} from '@/assets/svgs/logo-cat.svg'
import {ReactComponent as LogoSearch} from '@/assets/svgs/search.svg'
import './List.less'

interface Event {
    id: number,
    name: string,
    begin_time: string,
    end_time: string,
    description: string,
    images: Array<string>
    goings_count: number,
    likes_count: number,
    creator: {
      avatar: string,
      username: string
    },
    channel: {
      name: string,
      id: number 
    }
  }

interface Props {
    user: User,
    updateUser: Function
}

const mapStateToProps = (state: any) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        updateUser: (user: {}) => {
            const action = {
                type: 'user/updateUser',
                payload: user
            }
            dispatch(action)
        },
    }
}

class List extends Component<Props> {

    state = {
        events: [],
        animateType: '',
        selectedChannelTypes: ''
    }
    
    hasMore: boolean = true
    token: any = ''
    height: number = 0 // 列表的高度

    listRef = React.createRef<HTMLDivElement>()
    containerRef = React.createRef<HTMLDivElement>()
    
    // 列表页右滑
    rightSlip = () => {
        this.setState({animateType: 'right-slip'})
    }

    // 列表页左滑
    leftSlip = () => {
        this.setState({animateType: 'left-slip'})
        return false
    }

    //监听滚动事件，更新数据源events
    handleScroll = () => {
        const container = this.containerRef.current as HTMLElement
        //判断滚动条已滚过高度 + 容器高度是否已达到列表总高度
        if (container.scrollTop + container.offsetHeight >= this.height - 20) {
          this.updateEvents()
        }
    }

    //展示搜索结果
    handleSearch = (selectedDateType:string, selectedChannelTypes:string): void => {
        this.hasMore = true
        this.setState({selectedChannelTypes})
        const container = this.containerRef.current as HTMLElement
        container.scrollTop = 0
        setTimeout(() => {
          this.leftSlip()
          this.updateEvents(true)
        })
    }

    // 清除搜索结果
    handleClear = () => {
        this.setState({selectedChannelTypes: ''})
        this.hasMore = true
        const container = this.containerRef.current as HTMLElement
        container.scrollTop = 0
        setTimeout(() => {
          this.updateEvents(true)
        })
    }

    // 跳转Me页面
    goToMe = () => {
        history.push('/me')
    }

    //更新数据源events数组，扩充列表项
    updateEvents = async (clear:boolean=false) => {
        if (!this.hasMore) return
        this.token = this.token ? this.token : localStorage.getItem('user_token') as string
        const {events, selectedChannelTypes} = this.state
        const offset: number =  clear ? 0 : events.length
        const eventsRes = await reqEvents(this.token, offset, selectedChannelTypes)
        const newEvents = eventsRes.data.events
        if (clear) {
          this.setState({events: newEvents})
        } else {
          this.setState({events: [...events, ...newEvents]})
        }
        this.hasMore = eventsRes.data.hasMore
    }

    async componentDidMount() {
        this.token = this.token ? this.token : localStorage.getItem('user_token') as string
        if (!this.token) return
        //请求用户信息和数据源events
        const [userRes, eventsRes] = await Promise.all([reqUser(this.token), reqEvents(this.token)])
        if (userRes.status !== 200) {
          localStorage.removeItem('user_token')
          return this.setState({})
        }
        
        //储存用户信息
        this.props.updateUser(userRes.data)
        this.setState({events: eventsRes.data.events})
        this.hasMore = eventsRes.data.hasMore
    }

    componentDidUpdate() {
        //判断登录态
        const token = localStorage.getItem('user_token')
        if (!token) {   
            return <Login></Login>
        }
        // 每次组件更新完毕都获取列表的高度
        const listNode = this.listRef.current as HTMLElement
        this.height = listNode.offsetHeight + parseInt(getComputedStyle(listNode).marginTop)
    }

    render() {

        //判断登录态
        const token = localStorage.getItem('user_token')
        if (!token) {
            return <Login></Login>
        }
        const {animateType, selectedChannelTypes, events} = this.state
        
        return (
            <div 
            className={`main-container ${animateType}`}
            onScroll={debounce(this.handleScroll, 300)}
            ref={this.containerRef}>
                {/* 搜索页组件*/}
                <Search handleSearch={this.handleSearch}/>
                <div className='mask' onClick={this.leftSlip}></div>

                {/* 顶部栏 */}
                <div className="header">
                    <div className="header-navbar">
                        <LogoSearch className="icon-search" onClick={this.rightSlip}/>
                        <LogoCat className='icon-logo-cat'/>
                        <img src={this.props.user.avatar} alt="avatar" className='avatar-img' onClick={this.goToMe}/>
                    </div>
                    {/* 显示搜索结果 */}
                    {
                        selectedChannelTypes === '' ?
                        null
                        :
                        <div className="header-search-res">
                            <div className="header-search-res-top">
                            <span>{events.length} Results</span>
                            <button onClick={this.handleClear}>CLEAR SEARCH</button>
                            </div>
                            <div className="header-search-res-bottom">
                            Searched for Channel {selectedChannelTypes} Activities
                            </div>
                        </div>
                    }

                </div>
                
                
                {/* 列表 */}
                <div className={"events-container"} 
                ref={this.listRef}>
                    {events.map((event: Event) =>
                        <Item event={event} key={event.id} ></Item>
                    )}

                    <div className="no-found" style={{display: selectedChannelTypes!=''&&events.length===0?'block':'none'}}>
                        <span className="icon-no-activity"></span>
                        <span className='no-found-msg'>No activity found</span>
                    </div>]
                </div>   
            </div>
            
        ) 
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(List);