import React, {Component} from 'react'

import {reqToken, join} from '@/api/index'
import {getLocale, setLocale, formatMessage,} from 'umi'
import { history } from 'umi';
import Toast from '@/components/Toast/Toast'
// import { User } from '@/models/index'

import bgPic from '@/assets/images/Street-Dance-01.jpg'
import {ReactComponent as LogoCat} from '@/assets/svgs/logo-cat.svg'
import {ReactComponent as LogoUser} from '@/assets/svgs/user.svg'
import {ReactComponent as LogoPassword} from '@/assets/svgs/password.svg'
import './Login.less';


export default class Login extends Component {
  
  state = { 
    username: '',
    password: '',
    activeInput: '',
    lang: 'en',
    isToastShow: false
  }

  //注册
  register = () => {
    join().then((res) => 
       //console.log('注册成功')
       this.handleToastShow()
     ).catch((err) => console.log(err))
  }

  //输入框focus态
  handleFocus = (e: React.SyntheticEvent, activeInput: 'username' | 'password' | '') => {
    this.setState({activeInput: activeInput})
  }

  //输入框blur态
  handleBlur = (e: React.SyntheticEvent) => {
    this.setState({activeInput: ''})
  }
  
  // 处理输入数据的改变，更新对应的状态
  handleChange = (key: 'username' | 'password', val:string): void => {
    this.setState({[key]: val})
  }

  //语言切换
  changeLang = () => {
    const locale = getLocale();
    if (locale === 'en-US') {
      setLocale('zh-CN', false)
      this.setState({lang: 'zh'})
    } else {
      setLocale('en-US', false)
      this.setState({lang: 'en'})
    }
  }

  //显示toast
  handleToastShow = () => {
    this.setState({isToastShow: true})
    //定时销毁 Toast Dom
    setTimeout(() => {
        this.setState({
            isToastShow: false,  
        })
    }, 1000)
  }

  //登录
  signIn = async () => {
    const {username, password} = this.state
    const res = await reqToken({username, password})
    const {status} = res
    if (status === 200) {
      const {token, user} = res.data
      //console.log(user)

      //存放token
      localStorage.setItem('user_token', token)
      history.push('/list');
    } else {
      console.log(res.data.error)
    }
  }

  render() {

    return (
      <div className='login-container'>
        {/* 背景图 */}
        <img src={bgPic} alt="background" className='bg-pic'/>
        <div className='bg-filter'></div>
  
        {/* 头部模块 */}
        <div className='header'>
          {/* 标题 */}
          <h3 className='subtitle'>{formatMessage({id: 'subtitle'})}</h3>
          <h3 className='title'>{formatMessage({id: 'title'})}</h3>
       
          {/* 左上角注册按钮 */}
          <div className="register" onClick={this.register}>{this.state.lang==='en'?'注册':'Register'}</div>      
          {/* 右上角切换语言按钮  */}
          <div className="lang" onClick={this.changeLang}>{this.state.lang==='en'?'中文':'English'}</div>
  
          {/* 黑猫logo */}
          <div className="logo">
            <LogoCat className="icon-logo-cat"></LogoCat>
          </div>
        </div>

        <Toast isToastShow={this.state.isToastShow} toastText='注册成功!'></Toast>

        {/* 底部模块 */}
        <div className="footer">
         
         {/* 用户名输入框 */}
          <div className={"input-item input-item-user " + (this.state.activeInput==='username'?'active':'')} >
            <LogoUser className="input-item-img"></LogoUser>
            <input type="text"
              placeholder={this.state.lang === 'en' ? 'username' : '用户名'}
              onFocus={e => this.handleFocus(e, 'username')}
              onBlur={this.handleBlur}
              onChange={e => this.handleChange('username', e.target.value)}
            />
          </div>
  
          {/* 密码输入框 */}
          <div className={"input-item input-item-password " + (this.state.activeInput==='password'?'active':'')} >
            <LogoPassword className="input-item-img"></LogoPassword>
            <input type="password"
              placeholder={this.state.lang === 'en' ? 'password' : '密码'}
              onFocus={e => this.handleFocus(e, 'password')}
              onBlur={this.handleBlur}
              onChange={e => this.handleChange('password', e.target.value)}
            />
          </div>
  
          {/* 登录按钮 */}
          <button className='sign-in'
          onClick={this.signIn}>
              <h3>{formatMessage({id: 'signin'})}</h3>
          </button>
        </div>
      </div>
    );
  }
}
  