import React, { Component } from 'react'
import { connect, Link, history } from 'umi'
import { reqUserLikes, reqUserGoing, reqUserPast} from '@/api/index'
import Item from '@/components/ListItem/Item'
import Login from '@/pages/Login/Login'

import {ReactComponent as LogoCat} from '@/assets/svgs/logo-cat.svg'
import { ReactComponent as LogoHome } from '@/assets/svgs/home.svg'
import { ReactComponent as LogoEmail } from '@/assets/svgs/email.svg'
import { ReactComponent as NoActivity } from '@/assets/svgs/no-activity.svg'
import './Me.less'

interface Props {
    user: any
}

export class Me extends Component<Props> {

    state: any = {
        userLikes: {
            events: [],
            hasMore: false,
            total: 0
        },
        userGoing: {
            events: [],
            hasMore: false,
            total: 0
        },
        userPast: {
            events: [],
            hasMore: false,
            total: 0
        },
        selectedType : {
            events: [],
            hasMore: false,
            total: 0
        },
    }

    token: any = ''

    goToList = () => {
        history.push('/list')
    }

    goToMe = () => {
        history.push('/me')
    }

    //tab跳转likes
    selectLikes = () => {
        this.setState({selectedType: this.state.userLikes})
    }

    //tab跳转going
    selectGoing = () => {
        this.setState({selectedType: this.state.userGoing})
    }

    //tab跳转past
    selectPast = () => {
        this.setState({selectedType: this.state.userPast})
    }

    async componentDidMount () {

        this.token = this.token ? this.token : localStorage.getItem('user_token') as string
        if (!this.token) return
        //请求用户的likes，going，past数据
        const [resUserLikes, resUserGoing, resUserPast] = await Promise.all([reqUserLikes(this.token), reqUserGoing(this.token), reqUserPast(this.token)])
        const [userLikes, userGoing, userPast] = [resUserLikes.data, resUserGoing.data, resUserPast.data]
        this.setState({userLikes: userLikes, userGoing: userGoing, userPast: userPast, selectedType: userLikes})
        
    }

    render() {
        //判断登录态
        const token = localStorage.getItem('user_token')
        if (!token) {
            return <Login></Login>
        }
        
        const {user} = this.props
        
        return (
            <div className='me'>
                
                {/* 顶部栏 */}
                <div className="me-navbar">
                    <LogoHome className="icon-home" onClick={this.goToList}/>
                    <LogoCat className='icon-logo-cat'/>
                    <img src={this.props.user.avatar} alt="avatar" className='avatar-img' onClick={this.goToMe}/>
                </div>


                <div className='me-content'>
                    {/* 用户信息 */}
                    <div className='info'>
                                <img src={user.avatar} className='avatar'></img>
                                <p className='username'>{user.username}</p>
                                <div className='email'>
                                    <LogoEmail className='logo-email'></LogoEmail>
                                    <span className='text-email'>{user.email}</span>
                                </div>
                    </div>

                    {/* tab栏 */}
                    <div className='tab'>
                        <div className='tab-item' onClick={this.selectLikes}>
                            <img src={require('@/assets/svgs/like-outline.svg')} className='item-img'></img><span>Likes</span>
                        </div>
                        <div className='tab-item' onClick={this.selectGoing}>
                            <img src={require('@/assets/svgs/check-outline.svg')} className='item-img'></img><span>Going</span>
                        </div>
                        <div className='tab-item' onClick={this.selectPast}>
                            <img src={require('@/assets/svgs/past-outline.svg')} className='item-img'></img><span>Past</span>
                        </div>
                    </div>

                    {/* 列表 */}
                    <div className='list'>
                        {
                            this.state.selectedType.events.length === 0 ? 
                            <div className='no-activity'>
                                <NoActivity className='logo-no-activity'></NoActivity>
                                <p>no activity found</p>
                            </div> 
                            :
                            <div className='list-item'>
                                {
                                    this.state.selectedType.events.map((event: any) => 
                                        <Item event={event} key={event.id} ></Item>
                                    )
                                }
                            </div>
                        } 
                    </div>
                </div>
                

                
                    

                
            </div>
        )
    }
}

const mapStateToProps = (state: any) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(Me)
