import React, { Component } from "react";
import { connect, Link } from 'umi';
import {reqChannels} from '@/api/index'
import Login from '@/pages/Login/Login'

import './Search.less'
import {ReactComponent as LogoSearch} from '@/assets/svgs/search.svg'


interface Props {
    handleSearch: (selectedDateType:string, selectedChannelTypes:string) => void
}
  
interface ChannelTypes {
    [index: string]: {
        id: number,
        selected: boolean
    }
}

interface State {
    dateTypes: Array<string>,
    selectedDateType: string,
    channelTypes: ChannelTypes,
    searchButtonType: boolean,
  
}

export default class Search extends Component<Props> {

    state: State = {
        dateTypes: ['ANYTIME', 'TODAY', 'TOMORROW', 'THIS WEEK', 'THIS MONTH', 'LATER'],
        selectedDateType: '',
        channelTypes: {},
        searchButtonType: false,
    }

    channelSelectedCount: number = 0

    //点击全选标签
    handleAllChannel = () => {
        const {channelTypes} = this.state
        //将All标签取反，其余标签全部标记false，更新state
        Object.keys(channelTypes).forEach(channelType => {
        if (channelType === 'All') return
        channelTypes[channelType].selected = false
        })
        channelTypes['All'].selected = !channelTypes['All'].selected
        this.channelSelectedCount = 0
        this.setState({channelTypes, searchButtonType: true})
    }

    //点击某一频道标签
    handleChannel = (channelType: string) => {
        return () => {
          const {channelTypes} = this.state
          //被点击标签取反，All标签标记false，更新state
          channelTypes[channelType].selected = !channelTypes[channelType].selected
          channelTypes['All'].selected = false
          this.setState({channelTypes})
          this.channelSelectedCount += channelTypes[channelType].selected ? 1 : -1
          if (this.channelSelectedCount >= 1) {
            this.setState({searchButtonType: true})
          }
          else {
            this.setState({searchButtonType: false})    
          }
          
          //如果此时标签全部被选中，转入全选标签逻辑
          if (this.channelSelectedCount === Object.keys(channelTypes).length - 1) {
            setTimeout(() => {
              this.handleAllChannel()
            }, 100);
          }
        }
    }

    //点击某一日期标签
    handleDate = (selectedDateType: string) => {
        return () => {
            if (this.state.selectedDateType === selectedDateType) {
                this.setState({selectedDateType: '', searchButtonType: false})
            } else {
                this.setState({selectedDateType, searchButtonType: true})
            }
        }
    }

    handleSearch = () => {
        const {selectedDateType, channelTypes} = this.state
        const channelTypesArr = Object.keys(channelTypes)
        const selectedChannelTypes = channelTypes['All'].selected ?
        channelTypesArr.filter(channelType =>  channelType !== 'All').map(channelType => channelTypes[channelType].id)
        : channelTypesArr.filter(channelType => channelTypes[channelType].selected).map(channelType => channelTypes[channelType].id)
        
        this.props.handleSearch(selectedDateType, selectedChannelTypes.join(','))
    }


    async componentDidMount() {
        //判断登录态
        const token = localStorage.getItem('user_token') as string
        if (!token) {
            return <Login />
        }
        //判断是否有频道信息
        let {channelTypes} = this.state
        if (Object.keys(channelTypes).length > 0) return

        //向后台请求频道信息
        const channelsRes = await reqChannels(token)
       //储存频道信息
        channelTypes = channelsRes.data.channels.reduce((pre: ChannelTypes, channel: {name: string, id: number}) => ({
          ...pre,
          [channel.name]: {
            id: channel.id,
            selected: false
          }
        }), {
          'All': {
            id: -999,
            selected: false
          }
        })
        this.setState({channelTypes}) 
    }
    

    render() {

        const {dateTypes, channelTypes, selectedDateType} = this.state

        return (
            <div className='search'>
                {/* 选择时间 */}
                <h2 className="search-title">DATE</h2>
                <div className="search-types-container">
                {
                    dateTypes.map((dateType, i) => (
                    <span
                        key={i}
                        className={`date-item ${selectedDateType===dateType?'active':''}`}
                        onClick={this.handleDate(dateType)}
                    >
                        {dateType}
                        </span>
                    ))
                }
                </div>

                {/* 选择频道 */}
                <h2 className="search-title">CHANNEL</h2>
                <div className="search-types-container">
                {
                    Object.keys(channelTypes).map((channelType, i) => {
                    const {id, selected} = channelTypes[channelType]
                    return (
                        <span
                        key={id}
                        className={`channel-item ${selected ? 'active': ''}`}
                        onClick={channelType==='All' ? this.handleAllChannel : this.handleChannel(channelType)}
                        >
                        {channelType}
                        </span>
                    )
                    })
                }
                </div>

                {/* 搜索按钮 */}
                <button className={`search-button ${this.state.searchButtonType ? 'active': ''}`} onClick={this.handleSearch}>
                    <LogoSearch className='icon-search'></LogoSearch>
                    <span>SEARCH</span>
                </button>
            </div>
        )
    }
}