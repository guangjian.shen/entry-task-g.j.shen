import React, { Component } from 'react'
import { connect, Link, history } from 'umi'
import { RouteProps } from 'react-router-dom'
import { reqDetails, reqParticipants, reqLikes, reqComments } from '@/api/index'
import { User } from '@/models/index'
import Text from './components/Text/Text'
import When from './components/When/When'
import Where from './components/Where/Where'
import People from './components/People/People'
import CommentsList from './components/Comments/CommentsList'
import LikeGoing from './components/Like-going/LikeGoing'
import AddComments from './components/Comments/AddComments'
import Toast from '@/components/Toast/Toast'
import Login from '@/pages/Login/Login'

import { ReactComponent as LogoCat } from '@/assets/svgs/logo-cat.svg'
import { ReactComponent as LogoHome } from '@/assets/svgs/home.svg'
import { ReactComponent as LogoInfo } from '@/assets/svgs/info.svg'
import { ReactComponent as InfoOutline } from '@/assets/svgs/info-outline.svg'
import { ReactComponent as LogoPeople } from '@/assets/svgs/people.svg'
import { ReactComponent as PeopleOutline } from '@/assets/svgs/hpeople-outline.svg'
import { ReactComponent as LogoComment } from '@/assets/svgs/comment.svg'
import { ReactComponent as CommentOutline } from '@/assets/svgs/comment-outline.svg'
import './Details.less'

interface Props extends RouteProps {
    user: User
}

const mapStateToProps = (state: any) => {
    return {
        user: state.user
    }
}

class Details extends Component<Props>  {

    state= {
        event: {
            id: 0,
            name: '',
            begin_time: '',
            end_time: '',
            description: '',
            creator: {
                avatar: "",
                createdAt: "",
                email: "",
                id: 0,
                password: "f",
                salt: "",
                updatedAt: "",
                username: "user_4"
            },
            create_time: '',
            update_time: '',
            channel: {
                    createdAt: '',
                    id: 0,
                    name: '',
                    updatedAt: ''
            },
            images: [],
            location: '',
            location_detail: '',
            goings_count: 0,
            likes_count: 0,
            me_likes: false,
            me_going: false
        },
        participants: [],
        likes: [],
        comments: [],
        foot: 'likegoing',
        isToastShow: false,
        toastText: ''
    }

    token: any = ''

    //跳转List页面
    goToList = () => {
        history.push('/list')
    }

    //跳转Me页面
    goToMe = () => {
        history.push('/me')
    }

    //显示toast
    handleToastShow = (toastText: string) => {
        this.setState({isToastShow: true, toastText: toastText});
        // 定时销毁 Toast Dom
        setTimeout(() => {
            this.setState({
                isToastShow: false,  
            })
        }, 1000)
    }

    //隐藏comments组件，显示likesgoing组件
    onClose = () => {
        this.setState({foot: 'likegoing'})
    }

    //隐藏likegoing组件，显示comments组件
    onChat = () => {
        this.setState({foot: 'comment'})
    }

    //tab锚点跳转
    scrollToAnchor = (anchorName: string) => {
        if (anchorName) {
          //const tabBar  = document.getElementById('tab').offsetHeight
          let anchorElement = document.getElementById(anchorName);
          if (anchorElement) {
            anchorElement.scrollIntoView()
          }
        }
    }

    async componentDidMount() {

        this.token = this.token ? this.token : localStorage.getItem('user_token') as string
        if (!this.token) return
        const id = this.props.match.params.id
        //请求该页面event的likes，going和comments
        let [detailsRes, participantsRes, likesRes, commentsRes] = 
        await Promise.all([reqDetails(this.token, id), reqParticipants(this.token, id), reqLikes(this.token, id), reqComments(this.token, id)])
        //存储likes，going和comments
        let [detailsEvent, participants, likes, comments] = [detailsRes.data.event, participantsRes.data.users, likesRes.data.users, commentsRes.data.comments]
        this.setState({event: detailsEvent, participants: participants, likes: likes, comments: comments})

    }


    render () {

        //判断登录态
        const token = localStorage.getItem('user_token')
        if (!token) {
            return <Login></Login>
        }

        const {event, participants, likes, comments} = this.state

        return (
            <div className='details-container'>
                {/* 顶部栏 */}
                <div className="details-navbar">
                        <LogoHome className="icon-home" onClick={this.goToList}/>
                        <LogoCat className='icon-logo-cat'/>
                        <img src={this.props.user.avatar} alt="avatar" className='avatar-img' onClick={this.goToMe}/>
                </div>

                <Toast isToastShow={this.state.isToastShow} toastText={this.state.toastText}></Toast>

                {/* 详情页内容 */}
                <div className='details-content'>
                    {/* 详情页头部信息 */}
                    <div className='details-header'>
                        <div className='channel-name'>
                            {event.channel.name ? event.channel.name : ''}
                        </div>
                        <div className='title'>
                            {event.name ? event.name : ''}
                        </div>
                        <div className='creator'>
                            <img src={event.creator.avatar ? event.creator.avatar : ''} className='creator-avatar'/>
                            <div className='creator-username'>{event.creator.username ? event.creator.username : ''}</div>
                        </div>
                    </div>

                    {/* tab栏 */}
                    <div className='tab'>
                        <div className='tab-item' onClick={() => this.scrollToAnchor('text')}>
                            <LogoInfo className='item-img'></LogoInfo><span>Details</span>
                        </div>
                        <div className='tab-item' onClick={() => this.scrollToAnchor('people')}>
                            <LogoPeople className='item-img'></LogoPeople><span>Participants</span>
                        </div>
                        <div className='tab-item' onClick={() => this.scrollToAnchor('comments')}>
                            <LogoComment className='item-img'></LogoComment><span>Comments</span>
                        </div>
                    </div>

                    {/* 详情文本 */}
                    <div id='text'>
                        <Text event={event}></Text>
                        {/* when组件 */}
                        <When begin_time={event.begin_time} end_time={event.end_time}></When>
                        {/* where组件 */}
                        <Where location={event.location} location_detail={event.location_detail}></Where>
                    </div>


                    {/* 互动用户 */}
                    <div id='people'>
                        <People participants={participants} likes={likes}></People>
                    </div>
                    

                    {/* 评论列表 */}
                    <div id='comments'>
                        <CommentsList comments={comments}></CommentsList>
                    </div> 

                </div>
                
                {/* 底部栏组件 */}
                <div className='foot'>
                    <div style={{display: this.state.foot === 'likegoing' ? 'block' : 'none'}}>
                        <LikeGoing id={event.id} me_going={event.me_going} me_likes={event.me_likes} onChat={this.onChat}></LikeGoing>
                    </div>
                    <div style={{display: this.state.foot === 'comment' ? 'block' : 'none'}}>
                        <AddComments id={event.id} onClose={this.onClose} handleToastShow={this.handleToastShow}></AddComments>
                    </div>
                    
                </div>
                
            </div>
        )
    } 
}

export default connect(mapStateToProps)(Details);
