import React, { Component } from 'react'
import './People.less'

import { ReactComponent as CheckOutline } from '@/assets/svgs/check-outline.svg'
import { ReactComponent as LikeOutline } from '@/assets/svgs/like-outline.svg'

interface Props {
    participants: any[],
    likes: any[]
}

export default class People extends Component<Props> {

    state = {
        participants: [],
        likes: [],
        moreGoing: false,
        moreLikes: false
    }

    changeGoing = () => {
        this.setState({moreGoing: !this.state.moreGoing})
    }
    changeLikes = () => {
        this.setState({moreLikes: !this.state.moreLikes})
    }

    render () {
        
        let {participants, likes} = this.props
        let [goingCounts, likesCounts] = [participants.length, likes.length]

        return (
            <div className='people'>
                <div className='going-list'>                     
                    <div className='going-counts'>
                            <CheckOutline className='logo-going'></CheckOutline>
                            <span>{goingCounts}going</span>   
                    </div>
                    <div className={`goingAvatars ${this.state.moreGoing  ? 'more' : 'less'}`}>
                        <div className='button' onClick={this.changeGoing} style={{display: this.state.moreGoing || participants.length < 8 ? 'none' : 'block'}}>
                            <div className='arr-bottom'>&darr;</div>
                        </div>
                            {
                                participants.map((participant: any, index: number) => 
                                    <img src={participant.avatar} key={index}></img>
                                )
                            } 
                    </div>
                </div>
                <div className='likes-list'>
                    <div className='likes-counts'>
                            <LikeOutline className='logo-like'></LikeOutline>
                            <span>{likesCounts}likes</span>
                    </div> 
                    <div className={`likesAvatars ${this.state.moreLikes  ? 'more' : 'less'}`}>
                        <div className='button' onClick={this.changeLikes} style={{display: this.state.moreLikes || likes.length <  8 ? 'none' : 'block'}}>
                            <div className='arr-bottom'>&darr;</div>
                        </div>
                            {
                                likes.map((like: any, index: number) => 
                                    <img src={like.avatar} key={index}></img>
                                )
                            }
                    </div>
                </div>  
            </div>
        )
    }
}