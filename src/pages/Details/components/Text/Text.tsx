import React, { Component } from 'react'
import './Text.less'

interface Props {
    event: {
        id: number,
        name: string,
        begin_time: string,
        end_time: string,
        description: string,
        creator: {
            avatar: string,
            createdAt: string,
            email: string,
            id: number,
            password: string,
            salt: string,
            updatedAt: string,
            username: string
        },
        create_time: string,
        update_time: string,
        channel: {
                createdAt: string,
                id: number,
                name: string,
                updatedAt: string
        },
        images: any[],
        location: string,
        location_detail: string,
        goings_count: number,
        likes_count: number,
    }
}

export default class Text extends Component<Props> {
    render() {

        const {event} = this.props

        return (
            <div className='text'>
                <div className='images'>
                    {
                        event.images.map((image: string, index: number) => 
                                <img src={image} key={index}></img>
                    )}
                </div>
                <div className='description'>
                    <input type="checkbox" name="toggle" id="toggle" style={{display: 'none'}} />
                    <p >{event.description}</p>
                    <div className='view-all'>
                        <label htmlFor="toggle"></label>
                    </div>
                </div>      
            </div>
        )
    }
}
