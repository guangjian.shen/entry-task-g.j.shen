import React, { Component } from 'react'

import './When.less'

interface Props {
    begin_time: string,
    end_time: string
}

export default class When extends Component<Props> {


    render () {
        const month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        const begin_time = new Date(this.props.begin_time)
        const end_time = new Date(this.props.end_time)

        const begin_year = begin_time.getFullYear()
        const begin_month = month[begin_time.getMonth() + 1]
        const begin_day = begin_time.getDate()
        const begin_hour = begin_time.getHours();
        const begin_minute = begin_time.getMinutes();
        let [new_begin_hour, new_begin_minute, new_end_hour, new_end_minute] = ['', '', '', '']

        if (begin_hour < 10) {
            new_begin_hour = '0' + begin_hour.toString() 
        }  
        else {
            new_begin_hour = begin_hour.toString()
        }

        if (begin_minute < 10) {
            new_begin_minute = '0' + begin_minute.toString()
        }
        else {
            new_begin_minute = begin_minute.toString()
        }

        

        const end_year = end_time.getFullYear()
        const end_month = month[end_time.getMonth() + 1]
        const end_day = end_time.getDate()
        const end_hour = end_time.getHours();
        const end_minute = end_time.getMinutes();  

        if (end_hour < 10) {
            new_end_hour = '0' + end_hour.toString() 
        }  
        else {
            new_end_hour = end_hour.toString()
        }

        if (end_minute < 10) {
            new_end_minute = '0' + end_minute.toString()
        }
        else {
            new_end_minute = end_minute.toString()
        }

        const begin_text1 = begin_day + ' ' + begin_month + ' ' + begin_year
        const begin_text2 = new_begin_hour + ':' + new_begin_minute
        const end_text1 = end_day + ' ' + end_month + ' ' + end_year
        const end_text2 = new_end_hour + ':' + new_end_minute
        //console.log(begin_text1)
        return (
            <div className='when'>
                
                <div className='title'> 
                    <p>When</p>
                </div>

                <div className='time'>
                    <div className='time-text'>
                        <p className='text1'>
                            {begin_text1}
                        </p>
                        <p className='text2'>
                            {begin_text2}
                        </p>
                    </div>

                    <div className='time-text'>
                        <p className='text1'>
                            {end_text1}
                        </p>
                        <p className='text2'>
                            {end_text2}
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}