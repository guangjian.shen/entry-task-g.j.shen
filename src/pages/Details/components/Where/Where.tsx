import React, { Component } from 'react'

import './Where.less'

interface Props {
    location: string,
    location_detail: string
}

export default class Where extends Component<Props> {

    render() {
        return (
            <div className='where'>
                <div className='title'> 
                    <p>Where</p>
                </div>
                <p className='location'>
                    {this.props.location}
                </p>
                <p className='location_detail'>
                    {this.props.location_detail}   
                </p>
                <img src={require('@/assets/images/gmap.png')}></img>
            </div>
        )
    }
} 