import React, { Component } from 'react'
import './CommentsList.less'

interface Props {
    comments: any[]
}

export default class CommentsList extends Component<Props> {
    
    state = {
        comments: [],
        toastActive: false
    }

    commentsRef = React.createRef<HTMLDivElement>()

    handleScroll = ()=> {
        
        const comments = this.commentsRef.current as HTMLElement
        const scrollTop = comments.scrollTop;//滚动条滚动高度
        const clientHeight = comments.clientHeight;//可视区域高度
        const scrollHeight = comments.scrollHeight;//滚动内容高度
        const isBottom = scrollTop + clientHeight === scrollHeight
        if (isBottom) {
            this.setState({toastActive: true})
        } else {
            this.setState({toastActive: false})
        }
    }

    render () {
        let {comments} = this.props
        return (
            <div className='comments-list' onScroll={this.handleScroll} ref={this.commentsRef}>
                {
                    comments.map((comment: any) =>
                        <div className='comment' key={comment.id}>
                            <div className='comment-avatar'>
                                <img src={comment.user.avatar}></img>
                            </div>
                            <div className='comment-content'>
                                <div className='comment-user'>
                                    {comment.user.username}
                                </div>
                                <div className='comment-text'>
                                    {comment.comment}
                                </div>
                            </div>
                        
                        </div>
                    )
                }
                
            </div>
        )
    }
}

