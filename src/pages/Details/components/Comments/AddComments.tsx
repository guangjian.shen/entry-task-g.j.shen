import React, { Component } from 'react'
import { postComments } from '@/api/index'

import { ReactComponent as Cross } from '@/assets/svgs/cross.svg'
import { ReactComponent as Send } from '@/assets/svgs/send.svg'
import './AddComments.less'

interface Props {
    id: number
    onClose: () => void
    handleToastShow: (toastText: string) => void
}

export default class AddComments extends Component<Props> {

    state = {
        comments: ''
    }

    handleInput = (val: string) => {
        this.setState({comments: val})
        //console.log(this.state.comments)
    }

    handleSend = async () => {
        const token = localStorage.getItem('user_token')
        if (!token) return
        if (this.state.comments === '') return
        const resComments = await postComments(token, this.props.id, this.state.comments)
        //console.log(resComments)
        if (resComments.status === 200) {
            this.props.handleToastShow('message sent successfully!')
        }
    }

    render() {
        
        return (
            <div className='add-comments'>
                <div className='content'>
                    <div className='cross' onClick={() => this.props.onClose}>
                        <Cross className='logo-cross' onClick={this.props.onClose}></Cross>
                    </div> 
                    <div className='comments'>
                        <input type="text" placeholder='leave your comments here'
                        onChange={e => this.handleInput(e.target.value)}
                        />
                    </div>
                </div>
                
                <div className='send' onClick={this.handleSend}>
                    <Send className='logo-send'></Send>
                </div>
            </div>
        )
    }
}
