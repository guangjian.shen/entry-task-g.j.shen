import React, { Component } from 'react'
import { connect } from 'react-redux'
import {postParticipants, deleteParticipants, postLikes, deleteLikes} from '@/api/index'
import './LikeGoing.less'

interface Props {
    id: number
    me_likes: boolean,
    me_going: boolean,
    onChat: () => void
    // event: any
    // participants: any[],
    // likes: any[],
    // updateParticipants: Function,
    // updateLikes: Function
}

let pre_likes: boolean
let pre_going: boolean

export default class LikeGoing extends Component<Props> {

    
    pre_likes = this.props.me_likes
    pre_going = this.props.me_going
    
    state = {
        me_likes: this.props.me_likes,
        me_going: this.props.me_going,
    }
    

    static getDerivedStateFromProps (props: any) { 
        if (props.me_likes !== pre_likes || props.me_going !== pre_going) {
            pre_likes = props.me_likes
            pre_going = props.me_going
            return {
                me_likes: props.me_likes,
                me_going: props.me_going
            }
         }
        return null
    }

    changeGoing = async () => {
        const token = localStorage.getItem('user_token') as string
        if (!token) return
        const id = this.props.id
        if (this.state.me_going) {
            const resGoing = await deleteParticipants(token, id)
            //console.log('delete',resGoing)
            if (resGoing.status === 200) {
                this.setState({me_going: false})
            }
        } 
        else {
            const resGoing = await postParticipants(token, id)
            //console.log('post',resGoing)
            if (resGoing.status === 200) {
                this.setState({me_going: true})
            }
        }
    }

    changeLike = async () => {
        const token = localStorage.getItem('user_token') as string
        if (!token) return
        const id = this.props.id
        if (this.state.me_likes) {
            const resLikes = await deleteLikes(token, id)
            //console.log('delete',resLikes)
            if (resLikes.status === 200) {
                this.setState({me_likes: false})
            }
        } 
        else {
            const resLikes = await postLikes(token, id)
            //console.log('post',resLikes)
            if (resLikes.status === 200) {
                this.setState({me_likes: true})
            }
        }
    }

    render() {

        let {me_likes, me_going} = this.state

        return (
            <div className='like-going'>
                <div className='comment'>
                    <img src={require('@/assets/svgs/comment-single.svg')} className='comment-single' onClick={this.props.onChat}></img>   
                </div>
                <div className='like'>
                    <img src={me_likes ? require('@/assets/svgs/like.svg') : require('@/assets/svgs/like-outline.svg')} 
                    onClick={this.changeLike}
                    className='logo-like'></img>
                </div>
                <div className='going'>
                    <div>
                        <img src={me_going ? require('@/assets/svgs/check.svg') : require('@/assets/svgs/check-outline.svg')} 
                        onClick={this.changeGoing}
                        className='logo-check'></img>
                        <span>Join</span>
                    </div>
                </div>
            </div>
        )
    }
}

