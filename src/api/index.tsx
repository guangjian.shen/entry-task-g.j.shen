import request from "./request";

const prefix = 'http://127.0.0.1:3000/api/v1'

export const reqToken = (reqParams:{
    username:string,
    password:string
  }) =>
    request(`${prefix}/auth/token`, '', reqParams, 'POST')

export const reqUser = (token: string) => request(`${prefix}/user`, token)

export const reqEvents = (token:string, offset:number=0, channels:string='') => request(`${prefix}/events`, token, {offset, channels})

export const reqChannels = (token: string) => request(`${prefix}/channels`, token)

export const reqDetails = (token: string, id: any) => request(`${prefix}/events/${id}`, token)

export const reqParticipants = (token: string, id: any) => request(`${prefix}/events/${id}/participants`, token)

export const reqLikes = (token: string, id: any) => request(`${prefix}/events/${id}/likes`, token)

export const reqComments = (token: string, id: any) => request(`${prefix}/events/${id}/comments`, token)

export const postParticipants = (token: string, id: any) => request(`${prefix}/events/${id}/participants`, token, {},'POST')

export const deleteParticipants = (token: string, id: any) => request(`${prefix}/events/${id}/participants`, token, {},'DELETE')

export const postLikes = (token: string, id: any) => request(`${prefix}/events/${id}/likes`, token, {},'POST')

export const deleteLikes = (token: string, id: any) => request(`${prefix}/events/${id}/likes`, token, {},'DELETE')

export const reqUserLikes = (token: string) => request(`${prefix}/user/events`, token, {type: 'liked'})

export const reqUserGoing = (token: string) => request(`${prefix}/user/events`, token, {type: 'going'})

export const reqUserPast = (token: string) => request(`${prefix}/user/events`, token, {type: 'past'})

export const postComments = (token: string, id: any, comment: string) => request(`${prefix}/events/${id}/comments`, token, {comment},'POST')


export const join = () => request(`${prefix}/join`, '', {
  username: 'test',
  email: 'test@shopee.com',
  avatar: 'https://e.coding.net/static/fruit_avatar/Fruit-11.png',
  password: 'test'
}, 'POST')