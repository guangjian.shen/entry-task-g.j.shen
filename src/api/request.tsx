type Method = 'GET' | 'POST' | 'DELETE'

interface Data {
  [index: string]: any;
}

interface ReqObj {
  method: Method,
  body?: string,
  headers?: {
    [index: string]: any;
  }
}

export default function request(url: string, token: string, data: Data={}, method: Method='GET') {
  const reqObj: ReqObj = {method}
  reqObj.headers = {}
  if (token) reqObj.headers['X-BLACKCAT-TOKEN'] = token

  if (method === 'GET') {
    let params:string[] = []
    for (let key in data) {
      params.push(`${key}=${data[key]}`)
    }
    if (params.length > 0) url = url + '?' + params.join('&')
  } else if (method === 'POST') {
      reqObj.body = JSON.stringify(data)
      reqObj.headers['Content-Type'] = 'application/json'
    
  }
  return fetch(url, reqObj).then(async function (res) {
    const {status} = res
      const response = await res.text()
      try {
        const data = JSON.parse(response)
        return {
          status, data
        }
      } catch (error) {
        return {
          status, data: response
        }
      }
  })
}