export function transformTime(timeStr: string): string {
    const months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Spt","Oct","Nov","Dec"]
    const date = new Date(timeStr)
    const year = date.getFullYear()
    const day = date.getDay()
    const month = months[date.getMonth()]
    const hour = date.getHours()
    const min = date.getMinutes().toString().padStart(2, '0')
    return `${day} ${month} ${year} ${hour}:${min}`
  }
  
export function debounce(fn:Function, delay:number=500) {
    let timer: number|null = null
    return function (this: unknown) {
        if (timer) {
        clearTimeout(timer)
        }
        timer = window.setTimeout((...args: Array<unknown>) => {
        fn.apply(this, ...args)
        }, delay)
    }
}