import { Effect, Reducer, Subscription } from 'umi';

export interface User {
    id: any,
    username: string,
    email: string,
    avatar: string,
    likes_count: number,
    past_count: number,
    goings_count: number
}

// export interface Event {
//   id: number,
//   name: string,
//   begin_time: string,
//   end_time: string,
//   description: string,
//   images: Array<string>
//   goings_count: number,
//   likes_count: number,
//   creator: {
//     avatar: string,
//     username: string
//   },
//   channel: {
//     name: string,
//     id: number 
//   }
// }

interface UserModelType {
  namespace: 'user',
  state: {},
  reducers: {
    updateUser: Reducer,
    //updateParticipants: Reducer
  },
  effects: {
  },
  subscriptions: {  
  }
}
  
const UserModel: UserModelType = {
    namespace: 'user',
    state: {
      user: {},
      participants: [],
      likes: []
    },
    reducers: {
      updateUser(state, {payload: user}) {
        return {
          ...state.user,
          ...user,
        };
      },
     },
     effects: {   
    },
    subscriptions: {
    }
}

export default UserModel;