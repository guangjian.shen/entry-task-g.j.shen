import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    { path: '/', component: '@/pages/index' },
    { path: '/login', component: '@/pages/Login/Login' },
    { path: '/list', component: '@/pages/List/List' },
    { path: '/events/:id', component: '@/pages/Details/Details' },
    { path: '/me', component: '@/pages/Me/Me'}
  ],
  dva: {},
  locale: {
    default: 'zh-CN',
    antd: false,
    title: false,
    baseNavigator: true,
    baseSeparator: '-',
  },
});
