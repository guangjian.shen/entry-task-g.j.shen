# Entry Task

Shopee校招生入职项目，所用技术栈为TypeScript + React + UmiJs + DvaJs。

日报邮件中的项目链接为个人仓库，笔者已将entry task代码转移到公司仓库，链接如下：
https://git.garena.com/guangjian.shen/entry-task-g.j.shen


## 项目实现功能

项目产品为移动端APP，包含以下功能：

- 登录。用户需要登录来访问，未登录用户不允许访问，支持简易的注册功能。

- 浏览。登陆后，用户可以浏览内容，包括活动列表页，详情页和用户页。详情页可以查看活动详情，包括活动名称，时间，地点，互动用户，评论等。用户页包含用户个人信息。

- 搜索。用户可以在列表页，依据时间和频道对活动进行筛选。

- 互动。支持点赞功能(like)，评论功能(comments)，加入活动功能(going)。

## 项目常用指令

安装依赖

`npm install`

项目启动

`npm start`

打包项目

`npm build`

## 项目目录结构

```
.
├── README.md
├── mock
├── package-lock.json
├── package.json
├── src
│   ├── api //网络请求
│   │   ├── index.tsx
│   │   └── request.tsx
│   ├── assets  //静态资源
│   │   ├── images
│   │   └── svgs
│   ├── components  //公共组件
│   │   ├── ListItem
│   │   └── Toast
│   ├── locales //国际化
│   │   ├── en-US.ts
│   │   └── zh-CN.ts
│   ├── models  //状态管理
│   │   └── index.tsx
│   ├── pages   
│   │   ├── Details //详情页面
│   │   │   └── components  //详情页面组件
│   │   ├── List    //列表页面
│   │   ├── Login   //登录页面
│   │   ├── Me  //用户页面
│   │   ├── Search  //搜索页面
│   │   └── index.tsx   //入口文件
│   └── utils
│       └── index.tsx
├── tsconfig.json
├── typings.d.ts
└── yarn.lock
└── .umirc.ts
```
整个项目的入口为`src/index.tsx`文件，路由配置在`.umirc.ts`文件，路由指向的页面在`src/pages`文件夹下。

## 路由设计

本项目包含以下几个页面：

|  页面   | 功能  |
|  :----:  | :----: |
| Login  | 登录页面，实现用户注册以及登录 |
| List  | 项目的主页面，渲染列表项以及搜索结果 |
| Search  | 搜索页面，根据筛选条件进行搜索 |
| Detail  | 详情页面，展示当前event的详细数据 |
| Me  | 用户页面，展示用户个人信息 |

项目启动后默认加载List页面，可通过点击图标跳转至搜索页面，详情页面以及用户页面，反之亦可返回主页面。所有页面在进入时都会进行登录状态判定，若未登录则跳转至登录页面。

具体路由配置如下：
```
routes: [
    { path: '/', component: '@/pages/index' },
    { path: '/login', component: '@/pages/Login/Login' },
    { path: '/list', component: '@/pages/List/List' },
    { path: '/events/:id', component: '@/pages/Details/Details' },
    { path: '/me', component: '@/pages/Me/Me'}
  ]
```
## 状态管理
项目基于`DvaJs`设计公共store，主要用于存储用户信息，便于状态管理。

具体store设计如下：
```
const UserModel: UserModelType = {
    namespace: 'user',
    state: {
      user: {},
      participants: [],
      likes: []
    },
    reducers: {
      updateUser(state, {payload: user}) {
        return {
          ...state.user,
          ...user,
        };
      },
     },
     effects: {   
    },
    subscriptions: {
    }
}
```
组件通过`connect`方法从公共store获取数据，其输入参数包含`mapStateToProps`和`mapDispatchToProps`函数。组件也可以通过`mapDispatchToProps`函数中提供的接口派发`action`，修改公共store数据。

## 组件化/模块化设计

项目整体采用组件化/模块化的设计思想，根据页面划分文件结构，设计对应业务组件放在页面文件夹下；对于底层公用的组件及方法进行封装，便于复用。

其中核心设计为网络请求的封装，放在`src/api`文件夹下。`src/api/index.ts`文件向各组件暴露请求方法，而各请求方法又基于`index`文件内的`request`方法封装，该方法基于`fetch`方法进行封装。

`request`方法如下：
```
export default function request(url: string, token: string, data: Data={}, method: Method='GET') {
  const reqObj: ReqObj = {method}
  reqObj.headers = {}
  if (token) reqObj.headers['X-BLACKCAT-TOKEN'] = token

  if (method === 'GET') {
    let params:string[] = []
    for (let key in data) {
      params.push(`${key}=${data[key]}`)
    }
    if (params.length > 0) url = url + '?' + params.join('&')
  } else if (method === 'POST') {
      reqObj.body = JSON.stringify(data)
      reqObj.headers['Content-Type'] = 'application/json'
    
  }
  return fetch(url, reqObj).then(async function (res) {
    const {status} = res
      const response = await res.text()
      try {
        const data = JSON.parse(response)
        return {
          status, data
        }
      } catch (error) {
        return {
          status, data: response
        }
      }
  })
}
```


## 无限滚动列表实现

项目开发初期拟采用虚拟列表方案，但对笔者来说难度较大，开发进度受阻，为能够按时交付项目，选择无限滚动方案作为替代，项目结束后有时间再去优化。

虚拟列表的核心设计思想如下：

>首先计算列表`list`可滚动高度，单个列表项高度以及可视区域高度，从而得到可视区域单次渲染的列表项个数。监听容器的滚动事件，选择性地渲染`list`中固定数目的列表项，即可视区域列表`visibleList`。`visibleList`的第一个元素称为锚点元素`anchor`（初始化为列表`list`第一个元素），不断更新锚点元素`anchor`，计算可视区域列表`visibleList`的第一项索引`startIndex`和最后一项索引`endIndex`，得到`visibleList`真正渲染的数据。

无限滚动的设计思想如下：

>列表渲染数据源为`events`数组，监听容器的滚动事件，判定列表是否已经滚动到底（可根据已滚动高度 + 容器高度是否大于等于列表总高度来判定），若已触底则发起请求，将获取的新数据`newEvents`数组拼接到`events`数组后面，并更新列表的高度，从而不断更新渲染数据源，实现无限滚动的效果。

滚动监听函数如下：

```
handleScroll = () => {
        const container = this.containerRef.current as HTMLElement
        if (container.scrollTop + container.offsetHeight >= this.height - 20) {
          this.updateEvents()
        }
    }
```

更新数据源函数如下：

```
updateEvents = async (clear:boolean=false) => {
        if (!this.hasMore) return
        this.token = this.token ? this.token : localStorage.getItem('user_token') as string
        const {events, selectedChannelTypes} = this.state
        const offset: number =  clear ? 0 : events.length
        const eventsRes = await reqEvents(this.token, offset, selectedChannelTypes)
        const newEvents = eventsRes.data.events
        if (clear) {
          this.setState({events: newEvents})
        } else {
          this.setState({events: [...events, ...newEvents]})
        }
        this.hasMore = eventsRes.data.hasMore
    }
```

## 多语言功能实现

项目的多语言功能基于`UmiJs`的`locale`插件实现。在`src/locale`文件夹下配置多语言文件`en-US.ts`和`zh-CN.ts`，通过`getLocale`，`setLocale`方法读取修改国际化语言值，并改变组件内部判定值`this.state.lang`，通过`formatMessage`方法切换展示多语言数据。

切换语言函数如下：
```
changeLang = () => {
    const locale = getLocale();
    if (locale === 'en-US') {
      setLocale('zh-CN', false)
      this.setState({lang: 'zh'})
    } else {
      setLocale('en-US', false)
      this.setState({lang: 'en'})
    }
  }
```
切换语言按钮及组件内部展示如下：
```
<div className="lang" onClick={this.changeLang}>{this.state.lang==='en'?'中文':'English'}</div>
```
```
<h3 className='subtitle'>{formatMessage({id: 'subtitle'})}</h3>
<h3 className='title'>{formatMessage({id: 'title'})}</h3>
```
